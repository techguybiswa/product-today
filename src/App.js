import React, { Component } from "react";
import { Query, ApolloConsumer } from "react-apollo";
import gql from "graphql-tag";
import { Row, Col, Button, Spin, Card, Icon, Avatar } from "antd";
import logo from "./logo.svg";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import "./App.css";
import { Layout, Menu, Breadcrumb } from "antd";
import ProductList from "./components/ProductList";
import { DatePicker } from "antd";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ProductDetails from "./components/ProductDetails";
import LikedProducts from "./components/LikedProducts";

const { Header, Content, Footer } = Layout;

const client = new ApolloClient({
  uri: "https://api.producthunt.com/v2/api/graphql",
  headers: {
    Authorization: "Bearer " + "gSbul7u-Nn30UyYo8ReEagjmtTIt5Jag0BlK4iiJYwo"
  }
});

class App extends Component {
  render() {
    function goToLikedProducts() {
      window.location.href = "/products-liked";
    }
    function goToHome() {
      alert("HOMMEE");
      window.location.href = "/";
    }
    return (
      <Router>
        <Switch>
          <Layout>
            <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
              <div className="logo" />
              <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={["1"]}
                style={{ lineHeight: "64px" }}
              >
                <Menu.Item key="1">
                  <Link to={`/`}>Home</Link>
                </Menu.Item>
                <Menu.Item key="2" onClick={this.goToLikedProducts}>
                  <Link to={`/product-liked`}>Liked Products</Link>
                </Menu.Item>
                <Menu.Item key="3">Policy</Menu.Item>
              </Menu>
            </Header>
            <Content style={{ padding: "0 50px", marginTop: 64 }}>
              <h2 style={{ paddingTop: "20px" }}>Welcome to Product Today!</h2>
              <div style={{ background: "#fff", padding: 24, minHeight: 380 }}>
                <ApolloProvider client={client}>
                  <Route exact path="/" component={ProductList} />
                  <Route
                    exact
                    path="/product/:productId"
                    component={ProductDetails}
                  />
                  <Route
                    exact
                    path="/product-liked"
                    component={LikedProducts}
                  />
                </ApolloProvider>
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Made By Bisso ©2020 Created at India
            </Footer>
          </Layout>
        </Switch>
      </Router>
    );
  }
}

export default App;
