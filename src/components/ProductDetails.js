import React, { Component } from "react";
import { Query, ApolloConsumer } from "react-apollo";
import gql from "graphql-tag";
import { Row, Col, Button, Spin, Card, Icon, Avatar, DatePicker } from "antd";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import "../styles/style.scss";
import Comments from "./Comments";
const { Meta } = Card;
// 179731
const GET_PRODUCT_DETAILS = gql`
  query getProductDetails($id: ID!) {
    post(id: $id) {
      description
      name
      tagline
      thumbnail {
        url
      }
    }
  }
`;
class ProductDetails extends Component {
  constructor(props) {
    super();
    this.state = {
      id: null,
      btnState: "Like Me"
    };
  }
  isLikedItem = () => {
    if (localStorage.getItem("liked_products") != null) {
      let listOfLikedProducts = localStorage.getItem("liked_products");
      listOfLikedProducts = JSON.parse(listOfLikedProducts);

      console.log("listOfLikedProducts ", this.props.match.params.productId);
      listOfLikedProducts.map(eachItem => {
        if (this.props.match.params.productId == eachItem.id) {
          this.setState({
            btnState: "Liked"
          });
        }
      });
    }
  };
  componentDidMount = () => {
    this.setState({
      id: this.props.match.params.productId
    });
    this.isLikedItem();
  };
  likeProduct = (name, url) => {
    let eachLikedProduct = {
      name: name,
      url: url,
      id: this.state.id
    };
    if (localStorage.getItem("liked_products") != null) {
      var listOfLikedProducts = localStorage.getItem("liked_products");
      listOfLikedProducts = JSON.parse(listOfLikedProducts);
      listOfLikedProducts = [...listOfLikedProducts, eachLikedProduct];
    } else {
      var listOfLikedProducts = [];
      listOfLikedProducts.push(eachLikedProduct);
    }
    listOfLikedProducts = JSON.stringify(listOfLikedProducts);
    localStorage.setItem("liked_products", listOfLikedProducts);
    this.setState({
      btnState: "Liked"
    });
  };
  render() {
    return (
      <div>
        <Query
          query={GET_PRODUCT_DETAILS}
          variables={{
            id: this.state.id
          }}
        >
          {({ loading, error, data, refetch }) => {
            if (loading)
              return (
                <Row>
                  {/* <Col span={}>
                </Col> */}
                  <Col span={24}>
                    <Spin
                      spinning={true}
                      tip="Fetching the product details..."
                      style={{ padding: "20px" }}
                    >
                      {/* <Skeleton active avatar title /> */}
                      <div
                        style={{
                          width: "100%",
                          height: "100vh",
                          backgroundColor: "black"
                        }}
                      />
                    </Spin>
                  </Col>
                </Row>
              );
            if (error)
              return (
                <div>
                  <h1>Error in app</h1>
                </div>
              );
            return (
              <div>
                <Row>
                  <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                    <Card
                      style={{ width: 300 }}
                      cover={
                        <img alt="example" src={data.post.thumbnail.url} />
                      }
                    >
                      <Meta
                        title={data.post.name}
                        description={data.post.description}
                      />
                    </Card>
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                    <Comments id={this.state.id} />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                    <Button
                      type="primary"
                      disabled={this.state.btnState == "Liked"}
                      onClick={() =>
                        this.likeProduct(
                          data.post.name,
                          data.post.thumbnail.url
                        )
                      }
                      block
                    >
                      {this.state.btnState}
                    </Button>
                  </Col>
                </Row>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default ProductDetails;
