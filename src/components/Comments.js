import React, { Component } from "react";
import { Query, ApolloConsumer } from "react-apollo";
import gql from "graphql-tag";
import {
  Row,
  Col,
  Button,
  Spin,
  Card,
  Icon,
  Avatar,
  DatePicker,
  Comment
} from "antd";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import "../styles/style.scss";
const { Meta } = Card;
// 179731
// http://localhost:3000/product/177617
const GET_ALL_COMMENTS = gql`
  query getAllComments($id: ID!, $pointer: String!) {
    post(id: $id) {
      comments(first: 5, after: $pointer) {
        totalCount
        pageInfo {
          startCursor
          endCursor
          hasNextPage
        }
        edges {
          cursor
          node {
            body
          }
        }
      }
    }
  }
`;
class Comments extends Component {
  constructor(props) {
    super();
    this.state = {
      id: null,
      pointer: ""
    };
  }
  getMoreData = data => {
    this.setState({
      pointer: data
    });
  };
  componentDidMount = () => {
    this.setState({
      id: this.props.id
    });
  };

  render() {
    return (
      <div>
        <Query
          query={GET_ALL_COMMENTS}
          variables={{
            id: this.state.id,
            pointer: this.state.pointer
          }}
        >
          {({ loading, error, data, refetch, fetchMore }) => {
            if (loading)
              return (
                <Row>
                  <h1>Loading comments...</h1>
                </Row>
              );

            if (error)
              return (
                <div>
                  <h1>Some error occurred</h1>
                </div>
              );

            return (
              <div>
                <h2> Comments ({data.post.comments.totalCount}) </h2>
                <hr />

                {data.post.comments.edges.map(eachComment => (
                  <div>
                    <Comment content={eachComment.node.body} />
                    <hr />
                  </div>
                ))}
                {data.post.comments.pageInfo.hasNextPage && (
                  <Button
                    type="success"
                    onClick={() =>
                      this.getMoreData(data.post.comments.pageInfo.endCursor)
                    }
                    block
                  >
                    Next Comments
                  </Button>
                )}
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default Comments;
