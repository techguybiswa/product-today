import React, { Component } from "react";
import { Query, ApolloConsumer } from "react-apollo";
import gql from "graphql-tag";
import {
  Row,
  Col,
  Button,
  Spin,
  Card,
  Icon,
  Avatar,
  DatePicker,
  Result
} from "antd";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import "../styles/style.scss";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const { Meta } = Card;

const GET_LIST_OF_PRODUCTS = gql`
  query getData($fromDate: DateTime, $toDate: DateTime) {
    posts(postedAfter: $fromDate, postedBefore: $toDate) {
      pageInfo {
        startCursor
        hasNextPage
        endCursor
        hasPreviousPage
      }
      totalCount
      edges {
        cursor
        node {
          name
          tagline
          id
          createdAt
          commentsCount
          featuredAt
          thumbnail {
            type
            url
            videoUrl
          }
        }
      }
    }
  }
`;
class ProductList extends Component {
  constructor(props) {
    super();
    this.state = {
      currentDate: "",
      nextDate: "",
      limit: 10
    };
  }
  formatDate = d => {
    let date = new Date(d);
    let day = date.getDate();
    day = day < 10 ? "0" + day : day;
    let month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let year = date.getFullYear();
    return year + "-" + month + "-" + day;
  };
  getTomorrow = d => {
    const tomorrow = new Date(d);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return this.formatDate(tomorrow);
  };
  setDates = () => {
    let date = new Date();
    let nextDate = this.getTomorrow(date);
    date = this.formatDate(date);
    this.setState({
      currentDate: date,
      nextDate
    });
  };
  componentDidMount = () => {
    this.setDates();
  };
  handleDateChange = e => {
    let nextDate = this.getTomorrow(e.target.value);
    console.log("Current date: " + e.target.value);
    console.log("Next date: " + nextDate);

    this.setState({
      currentDate: e.target.value,
      nextDate
    });
  };
  render() {
    return (
      <div>
        <p style={{ margin: "2px" }}>
          Select a date{" "}
          <input
            type="date"
            value={this.state.currentDate}
            onChange={this.handleDateChange}
          />
        </p>
        <Query
          query={GET_LIST_OF_PRODUCTS}
          variables={{
            fromDate: this.state.currentDate,
            toDate: this.state.nextDate
          }}
        >
          {({ loading, error, data, refetch }) => {
            if (loading)
              return (
                <Row>
                  {/* <Col span={}>
                </Col> */}
                  <Col span={24}>
                    <Spin
                      spinning={true}
                      tip="Fetching today's products..."
                      style={{ padding: "20px" }}
                    >
                      {/* <Skeleton active avatar title /> */}
                      <div
                        style={{
                          width: "100%",
                          height: "100vh",
                          backgroundColor: "black"
                        }}
                      />
                    </Spin>
                  </Col>
                </Row>
              );
            if (error)
              return (
                <div>
                  <Result
                    status="warning"
                    title="Opps....Error maybe bad internet or Expired Auth token"
                    extra={
                      <Button type="primary" key="console">
                        Please try after sometime
                      </Button>
                    }
                  />
                </div>
              );
            return (
              <div>
                <Row>
                  {data.posts.edges.map(eachItem => (
                    <Col xs={24} sm={24} md={12} lg={6} xl={4}>
                      <Link to={`/product/${eachItem.node.id}`}>
                        <div className="p-container">
                          <div
                            style={{
                              backgroundImage: `url(${
                                eachItem.node.thumbnail.url
                              })`
                            }}
                            className="p-container__image"
                          />
                          {eachItem.node.name.substring(0, this.state.limit)}
                          {eachItem.node.id.substring(0, this.state.limit)}
                        </div>
                      </Link>
                    </Col>
                  ))}
                  {!data.posts.edges.length && (
                    <Result title="No products to show today. Please change the date" />
                  )}
                </Row>
              </div>
            );
          }}
        </Query>
      </div>
    );
  }
}

export default ProductList;
