import React, { Component } from "react";
import { Query, ApolloConsumer } from "react-apollo";
import gql from "graphql-tag";
import { Row, Col, Button, Spin, Card, Icon, Avatar, DatePicker } from "antd";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import "../styles/style.scss";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const { Meta } = Card;

class LikedProducts extends Component {
  constructor(props) {
    super();
    this.state = {
      listOfLikedProducts: []
    };
  }

  componentDidMount = () => {
    if (localStorage.getItem("liked_products") != null) {
      let listOfLikedProducts = localStorage.getItem("liked_products");
      listOfLikedProducts = JSON.parse(listOfLikedProducts);
      this.setState({
        listOfLikedProducts
      });
    }
  };

  render() {
    return (
      <div>
        {this.state.listOfLikedProducts.length > 0 &&
          this.state.listOfLikedProducts.map(eachItem => (
            <Col xs={24} sm={24} md={12} lg={6} xl={4}>
              <Link to={`/product/${eachItem.id}`}>
                <div className="p-container">
                  <div
                    style={{
                      backgroundImage: `url(${eachItem.url})`
                    }}
                    className="p-container__image"
                  />
                  {eachItem.name.substring(0, 18)}
                </div>
              </Link>
            </Col>
          ))}
        {!this.state.listOfLikedProducts.length && <h1>No liked products</h1>}
      </div>
    );
  }
}

export default LikedProducts;
